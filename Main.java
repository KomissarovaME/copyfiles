package copy_files;

import java.io.IOException;
import java.util.Scanner;

public class Main {
    /**
     * Scanner сделан статическим, так как не нужно будет его закрывать вызывая Scanner.close().
     */
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[]args){
        copyFile();
        copyFile();
        System.out.println("=(");
    }
    /*
    * Данный метод работает с пользователем запрашивая данные и выполняя вызов копирования файлов.
    */
    private static void copyFile() {
        System.out.println("Введите путь откуда взять файл: ");
        String inputFiles = scanner.nextLine();
        System.out.println("И куда его записать: ");
        String outputFiles = scanner.nextLine();
        try {
            WorkWithFiles filesInputStream= new WorkWithFiles(inputFiles);
            System.out.println("Подождите, идёт копирование!");
            FilesInputStream.writeToFile(outputFiles);
            System.out.println("Готово!Файл " + inputFiles + " скопирован.");
        }catch (IOException e) {
            e.printStackTrace();
        }
    }
}
